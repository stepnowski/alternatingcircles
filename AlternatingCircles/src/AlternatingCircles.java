import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.util.Random;

public class AlternatingCircles extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Random randomNumber = new Random();
		
		//get random numbers between 0 and 255 for red, green, blue,...twice
		int randomRed1 = randomNumber.nextInt(256);
		int randomGreen1 = randomNumber.nextInt(256);
		int randomBlue1 = randomNumber.nextInt(256);
		int randomRed2 = randomNumber.nextInt(256);
		int randomGreen2 = randomNumber.nextInt(256);
		int randomBlue2 = randomNumber.nextInt(256);
		
		
		// need width and height to determine size of circles and allows panel to be resized
		int width = getWidth();
		int height = getHeight();
		
		Color randomColor1 = new Color(randomRed1,randomGreen1,randomBlue1);
		Color randomColor2 = new Color(randomRed2,randomGreen2,randomBlue2);
		
		for(int i = 1;i<=5;i++)
		{	
			//alternate colors starting with this color, which resets each time
			g.setColor(randomColor1);
			if(i%2==0)
			{
				g.setColor(randomColor2);
			}
			//could improve readability here
			g.fillOval((((i-1)*width)/10),(((i-1)*height)/10), (width-((i-1)*width/5)), (height-((i-1)*height/5)));
		}
		
	}
	
}
