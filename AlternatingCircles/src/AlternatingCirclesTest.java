import javax.swing.JFrame;

public class AlternatingCirclesTest
{

	public static void main(String[] args)
	{
		AlternatingCircles panel = new AlternatingCircles();
		
		JFrame application = new JFrame();
		
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		application.add(panel);
		
		application.setSize(250, 250);
		
		application.setVisible(true);

	}

}
